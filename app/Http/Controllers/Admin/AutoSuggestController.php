<?php

namespace App\Http\Controllers\Admin;

use App\Models\AutoSuggest;
use Illuminate\Http\Request;
use App\Models\AutoSuggestLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AutoSuggestController extends Controller
{
    protected const TASK_LIST = 'task_list_item';

    protected const JOB_TITLE = 'job_title';

    protected const DOCUMENT = 'document_to_be_submit';

    protected const EDUCATION = 'education_qualification';

    protected const LANGUAGE_LIST = 'language_list';

    protected const RACE_LIST = 'race_list';

    protected const JOB_LITE_INTEREST = 'job_lite_interest';

    protected const PROFICIENCY = 'proficiency';

    protected const SKILL_SET = 'skill_set';

    protected const INSTITUTION = 'institution';

    protected const HIGER_EDUCATION = 'higer_level_education';
    
    protected const GENDER = 'gender';
    
    protected const RACE = 'race';
    protected const JOB_TYPE = 'job_type';
    
    protected const SKILLS = 'skills';
    protected const SKILL_GROUP = 'skill_group';
    protected const SKILL_TYPES = 'skill_types';
    protected const SKILL_LEVEL = 'skill_level';
    protected const RATING_SEGMENTS = 'rating_segments';
    protected const CANCELLATION_REASON = 'cancellation_reasons';

    public $arrayData;

    public function index(Request $request)
    {
        $search = $request->autosuggest_select;
        $search_field = $request->search_field ?? '';

        if ($request->title == static::TASK_LIST) {
            $this->arrayData = [
                'title' => 'Task List',
                'mainHeading' => 'TASK LIST',
                'keyValue' =>  static::TASK_LIST,
                'is_picture' => false,
            ];
        } else if ($request->title == static::JOB_TITLE) {
            $this->arrayData = [
                'title' => 'Job Title',
                'mainHeading' => 'JOB LIST',
                'keyValue' =>  static::JOB_TITLE,
                'is_picture' => false,
            ];
        } else if ($request->title == static::DOCUMENT) {
            $this->arrayData = [
                'title' => 'Document',
                'mainHeading' => 'DOCUMENT TO BE SUBMIT',
                'keyValue' =>  static::DOCUMENT,
                'is_picture' => true,
            ];
        } else if ($request->title == static::EDUCATION) {
            $this->arrayData = [
                'title' => 'Qualification',
                'mainHeading' => 'EDUCATION QUALIFICATION',
                'keyValue' =>  static::EDUCATION,
                'is_picture' => false,
            ];
        } else if ($request->title == static::LANGUAGE_LIST) {
            $this->arrayData = [
                'title' => 'Language',
                'mainHeading' => 'LANGUAGE LIST',
                'keyValue' =>  static::LANGUAGE_LIST,
                'is_picture' => false,
            ];
        } else if ($request->title == static::RACE_LIST) {
            $this->arrayData = [
                'title' => 'Race',
                'mainHeading' => 'RACE LIST',
                'keyValue' =>  static::RACE_LIST,
                'is_picture' => false,
            ];
        } else if ($request->title == static::JOB_TYPE) {
            $this->arrayData = [
                'title' => 'Job Type',
                'mainHeading' => 'JOB TYPE',
                'keyValue' =>  static::JOB_TYPE,
                'is_picture' => false,
            ];
        } else if ($request->title == static::JOB_LITE_INTEREST) {
            $this->arrayData = [
                'title' => 'Job Interest',
                'mainHeading' => 'JOB LITE INTERESTS',
                'keyValue' =>  static::JOB_LITE_INTEREST,
                'is_picture' => false,
            ];
        } else if ($request->title == static::PROFICIENCY) {
            $this->arrayData = [
                'title' => 'Proficiency',
                'mainHeading' => 'PROFICIENCY',
                'keyValue' =>  static::PROFICIENCY,
                'is_picture' => false,
            ];
        } else if ($request->title == static::SKILL_SET) {
            $this->arrayData = [
                'title' => 'Skill Sets',
                'mainHeading' => 'SKILL SETS',
                'keyValue' =>  static::SKILL_SET,
                'is_picture' => false,
            ];
        } else if ($request->title == static::INSTITUTION) {
            $this->arrayData = [
                'title' => 'Institution',
                'mainHeading' => 'INSTITUTIONS',
                'keyValue' =>  static::INSTITUTION,
                'is_picture' => true,
            ];
        } else if ($request->title == static::HIGER_EDUCATION) {
            $this->arrayData = [
                'title' => 'Higer Level Education',
                'mainHeading' => 'HIGER LEVEL EDUCATION',
                'keyValue' =>  static::HIGER_EDUCATION,
                'is_picture' => false,
            ];
        } else if ($request->title == static::GENDER) {
            $this->arrayData = [
                'title' => 'Gender',
                'mainHeading' => 'GENDER',
                'keyValue' =>  static::GENDER,
                'is_picture' => false,
            ];
        }  else if ($request->title == static::RACE) {
            $this->arrayData = [
                'title' => 'Race',
                'mainHeading' => 'RACE',
                'keyValue' =>  static::RACE,
                'is_picture' => false,
            ];
        }  else if ($request->title == static::SKILLS) {
            $this->arrayData = [
                'title' => 'Skills',
                'mainHeading' => 'SKILLS',
                'keyValue' =>  static::SKILLS,
                'is_picture' => false,
            ];
        }  else if ($request->title == static::SKILL_GROUP) {
            $this->arrayData = [
                'title' => 'Skill Group',
                'mainHeading' => 'SKILL GROUP',
                'keyValue' =>  static::SKILL_GROUP,
                'is_picture' => false,
            ];
        }  else if ($request->title == static::SKILL_TYPES) {
            $this->arrayData = [
                'title' => 'Skill Types',
                'mainHeading' => 'SKILL TYPES',
                'keyValue' =>  static::SKILL_TYPES,
                'is_picture' => false,
            ];
        }   else if ($request->title == static::SKILL_LEVEL) {
            $this->arrayData = [
                'title' => 'Skill Level',
                'mainHeading' => 'SKILL_LEVEL',
                'keyValue' =>  static::SKILL_LEVEL,
                'is_picture' => false,
            ];
        } else if ($request->title == static::RATING_SEGMENTS) {
            $this->arrayData = [
                'title' => 'Rating Segments',
                'mainHeading' => 'RATING SEGMENTS',
                'keyValue' =>  static::RATING_SEGMENTS,
                'is_picture' => false,
            ];
        }  else if ($request->title == static::CANCELLATION_REASON) {
            $this->arrayData = [
                'title' => 'Cancellation Reason',
                'mainHeading' => 'CANCELLATION REASON',
                'keyValue' =>  static::CANCELLATION_REASON,
                'is_picture' => false,
            ];
        } else {
            $this->arrayData = [
                'title' => '',
                'mainHeading' => '',
                'keyValue' =>  $request->title ?? '',
                'is_picture' => false,
            ];
        }

        if ($search_field) {
            $data = AutoSuggestLang::select('autosuggest_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['autosuggest_id'];
            }
            $autosuggestData = AutoSuggest::with('lang')->where('type', $this->arrayData['keyValue'])->whereIn('id', $da)->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $autosuggestData = AutoSuggest::with('lang')->where('type', $this->arrayData['keyValue'])->orderBy('created_at', 'desc')->paginate(20);
        }

        $arrayData = $this->arrayData;

        return view('admin.autosuggest.index', compact('arrayData', 'autosuggestData', 'search_field'));
    }
    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 2mb in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/autosuggest/', $image_name);
            } else {
                $image_name =  NULL;
            }
            $autosuggest = DB::transaction(function () use ($request, $image_name) {
                $autosuggest = AutoSuggest::create([
                    'status' => 'deactive',
                    'type' => $request->type,
                    'image_path' => $image_name,
                    'created_by' => Auth::user()->id,
                ]);
                $autosuggest->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'language' => 'en',
                    ],
                ]);
                return $autosuggest;
            });
            $msg = "Added successfully";
            if ($autosuggest) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $autosuggestData = AutoSuggest::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $autosuggestData,

        ];
    }

    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 2mb in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/autosuggest/', $image_name);
            } else {
                $image_name =  NULL;
            }

            $autosuggest = AutoSuggest::where('id', $request->id_pg)
                ->update([
                    'image_path' => $image_name,
                ]);

            $autosuggest = AutoSuggestLang::where('autosuggest_id', $request->id_pg)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                    ]
                );

            $msg = "Updated successfully";
            if ($autosuggest) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'active' ? 'deactive' : 'active';
        AutoSuggest::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return [
            "msg" => 'success'
        ];
    }

    public function destroy(Request $request)
    {
        $autosuggest = AutoSuggest::find($request->id);
        $autosuggest->lang()->delete();
        $autosuggest->delete();
        return [
            'msg' => 'success'
        ];
    }
    public function search(Request $req)
    {
        $lang = AutoSuggestLang::where('name', 'like', "%" . $req->search . "%")->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->autosuggest_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
