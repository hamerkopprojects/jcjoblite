@extends('layouts.master')
@section('content-title')
JOB CATEGORY
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_job_type">
    <i class="ti-plus"></i> Add New Category
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('job-category') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-4 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by Category Name/ Category ID">
                        </div>
<!--                        <div class="col-md-3 ml-0">
                            <select class="form-control search_val search_wid" name="select_maincategory" id="select_maincategory">
                                <option value="">Search by Parent Category</option>
                                @foreach($main_cat as $typelist)
                                <option value="{{$typelist->id}}" {{ $typelist->id ==  $cat_id  ? 'selected' : '' }}>{{$typelist->details[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>-->
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('job-category') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Job Category ID</th>
                                <th>Job Category Name</th>
                                <th>Description</th>
<!--                                <th>Parent Job Category</th>-->

                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($category) > 0)
                            @php
                            $i = 1;
                            @endphp
                   
                            @foreach ($category as $row_data)
                            <tr>
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->job_category_unique_id }}</td>
                                <td>{{ $row_data->details[0]->name ?? '' }}</td>
                                <td>{{ $row_data->details[0]->description }}</td>
<!--                                <td>{{ $row_data->getParentsNames() }}</td>-->
                                <td class="text-left">

                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                   
                                    <button type="button" class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Category" onclick="deleteCategory({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">

                </div>
            </div>
        </div>
    </div>
</div>

{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD JOB CATEGORY
                    </strong></h4>
                <a href="{{ route('job-category') }}" class="close">x</a>
            </div>
            <form id="user_form" action="javascript:;" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Job Category Name<span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter job type name" type="text" name="job_category_name" id="job_category_name" />
                        </div>
                       
<!--                        <div class="col-md-6">
                            <label class="control-label">Select parent category </label>
                            <select class="form-control" name="job_category" id="job_category">

                                <option value=""> Select Type </option>
                                @foreach($main_cat as $categorylist)
                                <option value="{{$categorylist->id}}">{{$categorylist->details[0]->name}}</option>
                                @if(count($categorylist->subcategory))
                                @include('admin.settings.job_category.subCategoryList',['subcategories' => $categorylist->subcategory])
                                @endif
                                @endforeach

                            </select>
                        </div>-->
                        <div class="col-md-6">
                            <label class="control-label">Description </label>
                            <textarea class="form-control area" name="description" id="description"></textarea>
                        </div>
                        <div class="col-md-6">

                            <label class="control-label">Add category image<span class="text-danger"></span></label>
                            <div class ="imgup">

                                <div class="scrn-link" >
                                    <button type="button" class="scrn-img-close delete-img" data-id="" onclick="removesrc_en()" data-type="app_image">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img id="previewimage_en" class="cover-photo" onclick="$('#uploadFile_en').click();" src="{{ asset('assets/images/upload.png') }}" />
                                </div>
                                <input type="file" id="uploadFile_en" name="category_image" style="visibility: hidden;" accept="image/*" value="" />
                                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                            </div>
                            <span class="error"></span>
                            <div class="catimg">
                                <p class="small">Max file size: 512KB</p><br>
                                <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p><br>
                                <p class="small" style="margin-top:-30px">File dimension: 1200 x 360 pixels</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-job-type" id="save_data">
                        ADD
                    </button>
                    <a href="{{ route('job-category') }}" class="btn btn-default reset_style">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}

@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
</style>
@endpush
@push('scripts')
<script>
    $('#add_new_job_type').on('click', function() {
        $('#name_change').html('Add Job Category');
        $('#save_data').text('Add').button("refresh");
        $("#user_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

    $('.save-job-type').on('click', function(e) {


        $("#user_form").validate({
            rules: {
                job_category_name: {
                    required: true,
                },      
            },
            messages: {
                job_category_name: {
                    required: "Job category name required",
                },
            },
            submitHandler: function(form) {
                user_unique = $("#user_unique").val();
                $('button:submit').attr('disabled', true);
                if (user_unique) {
                    $.ajax({
                        type: 'POST',
                        url: "{{route('job-category.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        url: "{{route('job-category.store')}}",
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                }
            }
        })
    });
    function removesrc_en(){
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
    };

    document.getElementById("uploadFile_en").onchange = function(e) {
        var focusSet = false;
        var reader = new FileReader();
        var fileUpload = document.getElementById("uploadFile_en");
        if (fileUpload!= '') {
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            var file_size = $('#uploadFile_en')[0].files[0].size;
            
                if (regex.test(fileUpload.value.toLowerCase()) == '') {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg or png </div>");
                
                }
                else if(file_size > 512000) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 512Kb in size </div>");
                }
            else if(fileUpload!= ''){
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

                    var fileUpload = document.getElementById("uploadFile_en");
                    var reader = new FileReader();

                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {

                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;

                            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                            var reader = new FileReader();
                        
                            document.getElementById("previewimage_en").src = e.target.result;
                        
                            reader.readAsDataURL(fileUpload.files[0]);
                        }           
                    }
                }
                else{  
                $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    var reader = new FileReader();
                    reader.onload = function(e) {
                    document.getElementById("previewimage_en").src = e.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                }
                
        }else{
            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
        
        }
    };

     $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        $('#name_change').html('Edit Job Category');
        $('#save_data').text('Save').button("refresh");
        var url = "job-category/edit/";
        $.get(url + page, function(data) {
            $('#job_category_name').val(data.category.details[0].name),
            $('#description').val(data.category.details[0].description),
            $('#job_category_ID').val(data.category.job_category_unique_id),
            $('#job_category').val(data.category.parent_id),
            $('#user_unique').val(data.category.id)
            $('#previewimage_en').val(data.category.details[0].image_path)
            var uploadsUrl = "<?php echo asset('/uploads/job_category') ?>";
            var imgurl = uploadsUrl + '/' + data.category.details[0].image_path;
            if (data.category.details[0].image_path) {
                $('#previewimage_en').attr("src", imgurl);
                $('#hidden_image_en').val(data.category.details[0].image_path);
            }
            
            $('#user_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function() {
        var cust_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Job category',
            content: 'Are you sure to ' + act_value + ' the Job Category?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('job-category.status.update')}}",
                        data: {
                            id: cust_id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("job-category")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

    
    function deleteCategory(id) {
        
        $.confirm({
            title: false,
            content: 'Are you sure to delete this job category? <br><br>You wont be able to revert this',
            
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('job-category.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("job-category")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }

</script>
@endpush