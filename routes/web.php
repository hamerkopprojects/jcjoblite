<?php

use App\Models\JobType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\JobTypeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashBoardController;
use App\Http\Controllers\UserVerificationController;
use App\Http\Controllers\Admin\AutoSuggestController;
use App\Http\Controllers\Admin\JobCategoryController;
use App\Http\Controllers\Admin\EditProfile;
use App\Http\Controllers\Admin\OtherSettingsController;
use App\Http\Controllers\Admin\UserManagementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');



Route::post("token/verify", [UserVerificationController::class, 'resetUserPassword'])->name('user.verify');
Route::post("verify", [UserVerificationController::class, 'verify'])->name('token.verify');
Route::post("user/password/change", [UserVerificationController::class, 'passwordUpdating'])->name('user.reset');
Route::get("user/logout", [UserVerificationController::class, 'logout'])->name('user.logout');
Route::get("/edit-user", [EditProfile::class, 'editUser'])->name('user.edit');
Route::post("/name-rest", [EditProfile::class, 'updateName'])->name('name.reset');
Route::post("/password-change", [EditProfile::class, 'updatePassword'])->name('password.new');


Route::get("/user", [UserManagementController::class, 'get'])->name('user-list.get');
Route::post("/user/store", [UserManagementController::class, 'store'])->name('user-list.store');
Route::get("/user-list/edit/{id}", [UserManagementController::class, 'edit'])->name('user-list.edit');
Route::post("/user-list/update", [UserManagementController::class, 'update'])->name('user-list.update');
Route::post("/user-list/status/update", [UserManagementController::class, 'statusUpdate'])->name('user-list.status.update');
Route::post("/user-list/delete", [UserManagementController::class, 'destroy'])->name('user-list.delete');
Route::post("/user-list/sendPassword", [UserManagementController::class, 'passwordSend'])->name('user-list.setPassword');

Route::get("/dashboard", [DashBoardController::class, 'get'])->name('dashboard.get');

// Pages
Route::get("/pages", [PagesController::class, 'get'])->name('pages.get');
Route::get("/pages/edit/{id}", [PagesController::class, 'edit'])->name('pages.edit');
Route::post("/pages/update", [PagesController::class, 'update'])->name('pages.update');
Route::post("/pages/delete", [PagesController::class, 'destroy'])->name('pages.delete');

// Faq
Route::get("/faqs", [FaqsController::class, 'get'])->name('faq.get');
Route::get("/faq/edit/{id}", [FaqsController::class, 'edit'])->name('faq.edit');
Route::post("/faqs/update", [FaqsController::class, 'update'])->name('faq.update');
Route::post("/faqs/store", [FaqsController::class, 'store'])->name('faq.store');
Route::post("/faqs/status-update", [FaqsController::class, 'statusUpdate'])->name('faq.status.update');
Route::post("/faqs/delete", [FaqsController::class, 'destroy'])->name('faq.delete');
Route::post("/faqs/search", [FaqsController::class, 'search'])->name('faq.search');

// Job Type
Route::get("/job-type", [JobTypeController::class, 'index'])->name('job-type');
Route::post("/job-type/store", [JobTypeController::class, 'store'])->name('job-type.store');
Route::post("/job-type/update", [JobTypeController::class, 'update'])->name('job-type.update');
Route::post("job-type/featured/update", [JobTypeController::class, 'featuredUpdate'])->name('job-type.featured.update');
Route::get("job-type/edit/{id}", [JobTypeController::class, 'edit'])->name('job-type.edit');
Route::post("job-type/status/update", [JobTypeController::class, 'statusUpdate'])->name('job-type.status.update');
Route::post("job-type/delete", [JobTypeController::class, 'destroy'])->name('job-type.delete');

// Job category
Route::get("/job-category", [JobCategoryController::class, 'index'])->name('job-category');
Route::post("/job-category/store", [JobCategoryController::class, 'store'])->name('job-category.store');
Route::post("/job-category/update", [JobCategoryController::class, 'update'])->name('job-category.update');
Route::get("job-category/edit/{id}", [JobCategoryController::class, 'edit'])->name('job-category.edit');
Route::post("job-category/status/update", [JobCategoryController::class, 'statusUpdate'])->name('job-category.status.update');
Route::post("job-category/delete", [JobCategoryController::class, 'destroy'])->name('job-category.delete');

// Auto Suggest
Route::get("/autosuggest/{title}", [AutoSuggestController::class, 'index'])->name('autosuggest');
Route::post("/autosuggest/store", [AutoSuggestController::class, 'store'])->name('autosuggest.store');
Route::get("/autosuggest/edit/{id}", [AutoSuggestController::class, 'edit'])->name('autosuggest.edit');
Route::post("/autosuggest/update", [AutoSuggestController::class, 'update'])->name('autosuggest.update');
Route::post("/autosuggest/status/update", [AutoSuggestController::class, 'statusUpdate'])->name('autosuggest.status.update');
Route::post("/autosuggest/delete", [AutoSuggestController::class, 'destroy'])->name('autosuggest.delete');
Route::post("/autosuggest/search", [AutoSuggestController::class, 'search'])->name('autosuggest.search');

// Category
Route::get("/category", [CategoryController::class, 'index'])->name('category');

Route::get("/other-settings", [OtherSettingsController::class, 'get'])->name('other.get');
Route::post("/other-settings/store", [OtherSettingsController::class, 'store'])->name('other.store');
