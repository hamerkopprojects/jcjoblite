<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeFieldAutosuggestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("ALTER TABLE autosuggest MODIFY COLUMN type ENUM('task_list_item', 'job_title', 'document_to_be_submit', 'education_qualification', 'language_list', 'race_list', 'job_lite_interest', 'proficiency', 'skill_set', 'institution', 'higer_level_education', 'gender', 'race','job_type','skills','skill_group','skill_types','skill_level','rating_segments','cancellation_reasons')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
