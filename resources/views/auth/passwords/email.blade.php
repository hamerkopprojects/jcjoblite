@extends('layouts.auth')

@section('auth-form')
<div class="login-form">
    <h4>FORGOT PASSWORD</h4>
    <h6 class="sub-title">Please enter your email to reset your password</h6>
    <form method="POST" action="{{ route('user.verify') }}">
        @csrf
        <div class="form-group">
            <label>Email *</label>
            <input placeholder="Enter email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
           
            @if($errors->any())  
                <strong class="error">{{$errors->first()}}</strong>
            @endif
        </div>
        <div class="action-btns">
            <a href="{{ route('login') }}" class="btn btn-default btn-flat m-b-15">BACK TO LOGIN</a>
            <button type="submit" class="btn btn-primary btn-flat m-b-15">Submit</button>
        </div>
    </form>
</div>
@endsection



{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
@push('css')
<style>
    .sub-title {
        color: #656565;
        margin-bottom: 20px;
    }

    .action-btns {
        display: flex;
        justify-content: space-between;
    }

    .action-btns .btn {
        width: 47%;
    }
    .error{
        color: red;
    }
</style>
@endpush