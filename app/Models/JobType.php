<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobType extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */


    protected $table = "job_type";

    public function lang()
    {
        return $this->hasMany(JobTypeLang::class, 'job_type_id')->withTrashed();
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\JobType', 'parent_id')->with('lang')->where('deleted_at', NULL);
    }

    public function getParentsNames()
    {
        if ($this->parent) {

            return $this->parent->getParentsNames() . " >> " . $this->parent->lang[0]->name;
        } else {
            return $this->name;
        }
    }
    public function subcategory()
    {

        return $this->hasMany('App\Models\JobType', 'parent_id')->with('lang')->where('deleted_at', NULL);
    }
}
