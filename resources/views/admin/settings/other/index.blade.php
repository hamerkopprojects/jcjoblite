@extends('layouts.master')

@section('content-title')
COMMON SETTINGS
@endsection
@section('content')
@include('user.flash-message')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
            <form id="name_reset_form" action="{{route('other.store')}}" method="post">
                @csrf
                                    <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Currency<span class="text-danger">*</span></label>
                                            @if(!empty($other->currency))
                                                <input type="text" class="form-control form-white" id="currency"
                                                    name="currency" value="{{ old('currency',$other->currency) }} " placeholder="Enter Currency">
                                                    @else
                                                    <input type="text" class="form-control form-white" id="currency"
                                                    name="currency" placeholder="Enter Currency" value="{{ old('currency') }}">
                                                    @endif
                                                    @error('currency')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Cancellation Time (Jobber) <span class="text-danger">*</span></label>
                                                @if(!empty($other->cancellation_time_jobber))
                                                <input type="text" class="form-control form-white" id="cancellation_time_jobber"
                                                    name="cancellation_time_jobber" placeholder="Enter Cancellation Time (Jobber)" value="{{ old('cancellation_time_jobber',$other->cancellation_time_jobber) }}">
                                                    @else
                                                    <input type="text" placeholder="Enter Cancellation Time (Jobber)" class="form-control form-white" id="cancellation_time_jobber"
                                                    name="cancellation_time_jobber" value="{{ old('cancellation_time_jobber') }}">
                                                    @endif
                                                    @error('cancellation_time_jobber')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Cancellation Time (Jobberator) <span class="text-danger">*</span></label>
                                                @if(!empty($other->cancellation_time_jobberator))
                                                <input type="text" class="form-control form-white" id="cancellation_time_jobberator"
                                                    name="cancellation_time_jobberator" placeholder="Enter Cancellation Time (Jobberator)" value="{{ old('cancellation_time_jobberator',$other->cancellation_time_jobberator) }}">
                                                    @else
                                                    <input type="text" placeholder="Enter Cancellation Time (Jobberator)" class="form-control form-white" id="cancellation_time_jobberator"
                                                    name="cancellation_time_jobberator" value="{{ old('cancellation_time_jobberator') }}">
                                                    @endif
                                                    @error('cancellation_time_jobberator')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Adverts <span class="text-danger">*</span></label>
                                                @if(!empty($other->adverts_status))
                                                    <button
                                                        type="button"
                                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ old('adverts_status',$other->adverts_status) }}"
                                                        value="{{ old('adverts_status',$other->adverts_status) }}"
                                                        aria-pressed="true"
                                                        autocomplete="off"
                                                        id="adverts_status">
                                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                    </button>
                                                    <input type="hidden" name="adverts_status" id="adverts_status_temp" value="{{ old('adverts_status',$other->adverts_status) }}"/>
                                                    @else
                                                    <button
                                                        type="button"
                                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 deactive"
                                                          value="deactive"
                                                        aria-pressed="true"
                                                        autocomplete="off"
                                                        id="adverts_status">
                                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                    </button>
                                                    <input type="hidden" name="adverts_status" id="adverts_status_temp" value="deactive"/>
                                                    @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Google Ads <span class="text-danger">*</span></label>
                                                @if(!empty($other->googleads_status))
                                                    <button
                                                        type="button"
                                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ old('adverts_status',$other->googleads_status) }}"
                                                        value="{{ old('adverts_status',$other->googleads_status) }}"
                                                        aria-pressed="true"
                                                        autocomplete="off"
                                                        id="googleads_status">
                                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                    </button>
                                                    <input type="hidden" name="googleads_status" id="googleads_status_temp" value="{{ old('googleads_status',$other->googleads_status) }}"/>
                                                    @else
                                                    <button
                                                        type="button"
                                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 deactive"
                                                          value="deactive"
                                                        aria-pressed="true"
                                                        autocomplete="off"
                                                        id="googleads_status">
                                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                    </button>
                                                    <input type="hidden" name="googleads_status" id="googleads_status_temp" value="deactive"/>
                                                    @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Payment Gateway <span class="text-danger">*</span></label>
                                                @if(!empty($other->paymentgateway_status))
                                                    <button
                                                        type="button"
                                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ old('adverts_status',$other->paymentgateway_status) }}"
                                                        value="{{ old('adverts_status',$other->paymentgateway_status) }}"
                                                        aria-pressed="true"
                                                        autocomplete="off"
                                                        id="paymentgateway_status">
                                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                    </button>
                                                    <input type="hidden" name="paymentgateway_status" id="paymentgateway_status_temp" value="{{ old('paymentgateway_status',$other->paymentgateway_status) }}"/>
                                                    @else
                                                    <button
                                                        type="button"
                                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 deactive"
                                                          value="deactive"
                                                        aria-pressed="true"
                                                        autocomplete="off"
                                                        id="paymentgateway_status">
                                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                    </button>
                                                    <input type="hidden" name="paymentgateway_status" id="paymentgateway_status_temp" value="deactive"/>
                                                    @endif
                                            </div>
                                </div>
                            <div class="col-lg-12">
                                <div class="row justify-content-end pb-5">
                                    <div class="col-md-12 filter-by-boat-jalbooth-butn01 text-md-left">
                                        <button type="submit" class="btn btn-primary buttonxl mr-md-2">
                                            Save
                                        </button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    if ($("#name_reset_form").length > 0) {
        $.validator.addMethod('minStrict', function (value, el, param) {
            return this.optional( el ) || value >= param && !(value % 1);
        });

        $("#name_reset_form").validate({
            rules: {
                currency: {
                    required: true,    
                },
                cancellation_time_jobber: {
                    required: true,
                    number: true
                }, 
                cancellation_time_jobberator: {
                    required: true,
                    number: true,
                },  
            },
            messages: {
                currency: {
                    required: "Currency is required",
                },
                cancellation_time_jobber: {
                    required: "Cancellation Time (Jobber) is required",
                    number: "Accept only numeric value",
                }, 
                cancellation_time_jobberator: {
                    required: "Cancellation Time (Jobberator) is required",
                    number: "Accept only numeric value",
                },       
            },
        })
    } 
    $('.change-status').on('click',function(){
      var activate = $(this).val();
      if (activate == 'active') {
        $(this).removeClass('active').addClass('deactive');
        $(this).val('deactive'); 
        $(this).siblings().val('deactive');
      } else {
        // $(this).removeClass('focus').addClass('active');
        $(this).removeClass('deactive').addClass('active');
        $(this).val('active');   
        $(this).siblings().val('active');
      }
    });
</script>
@endpush