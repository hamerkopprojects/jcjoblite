@extends('layouts.master')
@section('content')
@section('content-title')
        SUMMARY
@endsection
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-3 right-pos" >
            <label>Select date</label>
            <input type="text" class="form-control" name="dates">
            <i class="fa fa-calendar icon-style"></i>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="fa fa-files-o"></i>
            </div>
            <p class="card-category"></p>
            <h3 class="card-title">Job Sales
            </h3>
            <p >R 100000</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              {{-- <i class="material-icons text-danger">warning</i>
              <a href="javascript:;">Get More Space...</a> --}}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="fa fa-usd"></i>
            </div>
            <p class="card-category"></p>
            <h3 class="card-title">Adverts Sales</h3>
            <p>R 300000</p>
          </div>
          <div class="card-footer">
            {{-- <div class="stats">
              <i class="material-icons">date_range</i> Last 24 Hours
            </div> --}}
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="fa fa-sign-in"></i>
            </div>
            <p class="card-category"></p>
            <h3 class="card-title">Job application</h3>
            <p> 200</p>
          </div>
          <div class="card-footer">
            {{-- <div class="stats">
              <i class="material-icons">local_offer</i> Tracked from Github
            </div> --}}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-8 p-r-0 title-margin-right">
        {{-- <div class="page-header"> --}}
            <div class="page-title">
                <h5>Latest Adverts</h5>
            </div>
        {{-- </div> --}}
    </div>
    <div class="row">
    
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SNO.</th>
                                <th>Job Code</th>
                                <th>Job Title</th>
                                <th>Job Type</th>
                                <th>Plan</th>
                                <th>Jobberator</th>
                                <th>Expiry</th>
                                <th width="15%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>1</td>  
                              <td>JL-1001</td> 
                              <td>Capentry</td> 
                              <td>In-Person</td> 
                              <td>Premium</td> 
                              <td>Jhonson</td> 
                              <td>20-01-2021</td> 
                              <td class="text-center">
                                  <a href="#" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                  <a href="#" class="btn btn-sm btn-info text-white" title="View"><i class="fa fa-times"></i></a>
                              </td> 
                            </tr>
                               <td>2</td>  
                              <td>JL-1002</td> 
                              <td>Capentry</td> 
                              <td>Online</td> 
                              <td>Basic</td> 
                              <td>Anwar</td> 
                              <td>20-01-2021</td> 
                              <td class="text-center">
                                  <a href="#" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                  <a href="#" class="btn btn-sm btn-info text-white" title="View"><i class="fa fa-times"></i></a>
                              </td> 
                            </tr>  
                            </tr>
                               <td>3</td>  
                              <td>JL-1003</td> 
                              <td>Website development</td> 
                              <td>Online</td> 
                              <td>Free</td> 
                              <td>Freddy</td> 
                              <td>20-01-2021</td> 
                              <td class="text-center">
                                  <a href="#" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                  <a href="#" class="btn btn-sm btn-info text-white" title="View"><i class="fa fa-times"></i></a>
                              </td> 
                            </tr>                             
                        </tbody>
                    </table>
                </div>
            </div>
            <span class="see-more"><a href="#" class="href-style"> See more</a></span>
        </div>
    </div>
  </div>
@endsection
@push('css')
<link href="{{asset('/assets/css/dashboard.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .right-pos{
        position: absolute;
        right: 35px;
        top: 87px;
    }
    .icon-style{
        position: absolute;
    right: 28px;
    top: 50px;
    }
    .see-more{
        right: 38px;
        position: absolute;
        bottom: 7px;
    }
    .href-style{
       color:  cornflowerblue;
    }
</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    
$('input[name="dates"]').daterangepicker();
</script>
    
@endpush