<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobType;
use App\Models\JobTypeLang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class JobTypeController extends Controller
{
    public function index(Request $req)
    {
        $category = JobType::with('lang')->get();
        $main_cat = JobType::with('lang')
            ->where('parent_id', NULL)
            ->get();
        $search = $req->search ?? '';
        $search_catid = $req->search_catid ?? '';
        $cat_id = $req->select_maintype ?? '';

        $query = JobType::query();
        if (!empty($search)) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%")
                    ->orWhere('job_type_unique_id', 'like', "%" . $search . "%");
            });
        }
        if (!empty($cat_id)) {
            $cats = array();
            $cats = JobType::select('id')->with('lang')
                ->where('parent_id', $cat_id)
                ->get();
            $new_array = [];
            foreach ($cats as $catss) {
                $new_array[] = $catss['id'];
            }
            $query->whereIn('parent_id', $new_array)->orWhere('parent_id', $cat_id);
        }
        $query->with('lang');

        if (!empty($search_catid)) {
            $query->where('job_type_unique_id', $search_catid);
        }
        $category = $query->paginate(20);
        return view('admin.settings.job_type.index', compact('category', 'main_cat', 'search_catid', 'cat_id', 'search'));
    }
    public function store(Request $req)
    {
        $rules = [
            'job_type_name' => 'required',
            'type_image_en' => 'image|mimes:png,jpg,jpeg|max:2048',
            // 'description' => 'required',

        ];
        $messages = [
            'job_type_name.required' => 'Job type is required.',
            'type_image_en.max' => 'The image must be less than 2mb in size',
            'type_image_en.mimes' => "The image must be of the format jpeg or png",
            // 'type_image_en.dimensions' => "The image must be in  1200 x 360 pixels",


        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if (!empty($req->file('type_image_en'))) {
                $file = $req->file('type_image_en');
                $name_en = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/job_type/', $name_en);
            } else {
                $name_en =  NULL;
            }
            $userData = JobType::create([
                'job_type_unique_id' => $this->generateCategoryId(),
                'status' => "deactive",
                'is_featured' => "no",
                'parent_id' => $req->job_type
            ]);

            $userData->lang()->createMany([
                [
                    'name' => $req->job_type_name,
                    'description' => $req->description,
                    'image_path' => $name_en,
                    'language' => 'en',
                ],
            ]);

            $msg = "Job type added successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function generateCategoryId()
    {
        $lastcategory = JobType::select('job_type_unique_id')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastcategory) {
            $lastId = (int) substr($lastcategory->job_type_unique_id, 4);
        }

        $lastId++;

        return 'JOB' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }
    public function featuredUpdate(Request $req)
    {
        $status = $req->status === 'yes' ? 'no' : 'yes';

        $statusUpdate = JobType::where('id', $req->id)
            ->update([
                'is_featured' => $status
            ]);

        if ($req->status === 'yes') {
            return response()->json(['status' => 1, 'message' => 'Remove featured job type.']);
        } else {
            return response()->json(['status' => 1, 'message' => 'Make featured job type.']);
        }
    }
    public function edit($id)
    {
        $category = JobType::with('lang')->with('parent')->where('id', $id)->first();

        return [
            'category' => $category
        ];
    }

    public function update(Request $req)
    {

        $rules = [
            'job_type_name' => 'required',
            // 'type_image_en' => 'image|mimes:png,jpg,jpeg|max:512|dimensions:width=1200,height=360'
        ];
        $messages = [
            'job_type_name.required' => 'Job type is required.',
            // 'type_image_en.max' => 'The image must be less than 512Kb in size',
            // 'type_image_en.mimes' => "The image must be of the format jpeg or png",
            // 'type_image_en.dimensions' => "The image must be in  1200 x 360 pixels!",


        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($req->file('type_image_en'))) {
                $file = $req->file('type_image_en');
                $name_en = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/job_type/', $name_en);
            } else {
                $name_en =  $req->hidden_image_en;
            }
            $userData = JobType::where('id', $req->user_unique)
                ->update([
                    'is_featured' => "no",
                    'parent_id' => $req->job_type
                ]);

            $userData = JobTypeLang::where('job_type_id', $req->user_unique)->where('language', 'en')
                ->update([
                    'name' => $req->job_type_name,
                    'description' =>  $req->description,
                    'image_path' => $name_en,
                ]);

            $msg = "Job Type updated successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function statusUpdate(Request $req)
    {
        $att = JobType::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                JobType::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                JobType::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function destroy(Request $req)
    {

        $category = JobType::find($req->id);
        if (!empty($category)) {

            if (!empty($category->lang[0]->image_path)) {
                $path = $category->lang[0]->image_path;
                $file = public_path('/uploads/job_type/' . $path);
                $img = unlink($file);
            }
            $category->lang()->delete();
            $category->delete();
            $userData = JobType::where('parent_id', $req->id)
                ->update([
                    'parent_id' => NULL
                ]);
            return response()->json(['status' => 1, 'message' => 'Job type deleted successfully']);
            // $product = Product::select('id')->where('category_id', $req->id)->exists();
            // $attribute = Attribute::select('id')->where('category_id', $req->id)->exists();

            // if ($product == false && $attribute == false) {
            //     if (!empty($category->lang[0]->image_path)) {
            //         $path = $category->lang[0]->image_path;
            //         $file = public_path('/uploads/category/' . $path);
            //         $img = unlink($file);
            //     }
            //     $category->lang()->delete();
            //     $category->delete();
            //     $userData = Category::where('parent_id', $req->id)
            //         ->update([
            //             'parent_id' => NULL
            //         ]);
            //     return response()->json(['status' => 1, 'message' => 'Category deleted successfully']);
            // } else {

            //     return response()->json(['status' => 0, 'message' => 'You cannot delete,have related records']);
            // }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }
}
