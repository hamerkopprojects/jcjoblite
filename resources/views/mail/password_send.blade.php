@component('mail::message')
## Dear {{ucfirst($data['user']->name )?? ''}}

@component('mail::panel')
<p>We have created a one time password for this email account.Please sign in to  the accound and  set a new, permanent password</p>
<h3> Password : {{$data['password']}}</h3>
<h3>Email :{{$data['user']->email}}</h3>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
