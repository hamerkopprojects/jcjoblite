<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobCategory extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */


    protected $table = "job_category";

    public function details()
    {
        return $this->hasMany(JobCategoryDetails::class, 'job_cate_id')->withTrashed();
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\JobCategory', 'parent_id')->with('details')->where('deleted_at', NULL);
    }

    public function getParentsNames()
    {
        if ($this->parent) {
            return $this->parent->getParentsNames() . " >> " . $this->parent->details[0]->name;
        } else {
            return $this->name;
        }
    }
    public function subcategory()
    {

        return $this->hasMany('App\Models\JobCategory', 'parent_id')->with('details')->where('deleted_at', NULL);
    }
}
