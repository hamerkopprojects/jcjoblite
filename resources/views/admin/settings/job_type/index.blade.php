@extends('layouts.master')
@section('content-title')
JOB TYPE
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_job_type">
    <i class="ti-plus"></i> Add New Job Type
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('job-type') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-4 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by Type Name/ Type ID">
                        </div>
<!--                        <div class="col-md-3 ml-0">
                            <select class="form-control search_val search_wid" name="select_maintype" id="select_maintype">
                                <option value="">Search by Parent Type</option>
                                @foreach($main_cat as $typelist)
                                <option value="{{$typelist->id}}" {{ $typelist->id ==  $cat_id  ? 'selected' : '' }}>{{$typelist->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>-->
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('job-type') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Job Type ID</th>
                                <th>Job Type Name</th>
                                <th>Description</th>
<!--                                <th>Parent Job Type</th>-->

                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($category) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($category as $row_data)
                            <tr>
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->job_type_unique_id }}</td>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->lang[0]->description }}</td>
<!--                                <td>{{ $row_data->getParentsNames() }}</td>-->
                                <td class="text-left">

                                    {{-- <a href=" " class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-cube"></i></a> --}}
                                    {{-- <a href="{{ route('attribute', ['cat_id' => $row_data->id]) }}" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-cube"></i></a> --}}

                                    {{-- <a class="btn btn-sm {{ $row_data->is_featured == 'yes' ? 'btn-fea' : 'btn-danger' }} text-white set_featured" title="{{ $row_data->is_featured == 'yes' ? 'Remove from featured job type' : 'Make this featured job type' }}" data-status="{{$row_data->is_featured}}" data-id="{{ $row_data->id }}"><i class="fa fa-star"></i></a> --}}

                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                   
                                    <button type="button" class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Category" onclick="deleteCategory({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">

                </div>
            </div>
        </div>
    </div>
</div>

{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD JOB TYPE
                    </strong></h4>
                <a href="{{ route('job-type') }}" class="close">x</a>
            </div>
            <form id="user_form" action="javascript:;" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Job Type Name (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter job type name" type="text" name="job_type_name" id="job_type_name" />
                        </div>
                       
<!--                        <div class="col-md-6">
                            <label class="control-label">Select parent Type </label>
                            <select class="form-control" name="job_type" id="job_type">

                                <option value=""> Select Type </option>
                                @foreach($main_cat as $categorylist)
                                <option value="{{$categorylist->id}}">{{$categorylist->lang[0]->name}}</option>
                                @if(count($categorylist->subcategory))
                                @include('admin.settings.job_type.subTypeList',['subcategories' => $categorylist->subcategory])
                                @endif
                                @endforeach

                            </select>
                        </div>-->
                        <div class="col-md-6">
                            <label class="control-label">Description </label>
                            <textarea class="form-control area" name="description" id="description"></textarea>
                        </div>
<!--                        <div class="col-md-6">

                            <label class="control-label">Add type image (EN) <span class="text-danger"></span></label>
                            <div class ="imgup">

                                <div class="scrn-link" >
                                    <button type="button" class="scrn-img-close delete-img" data-id="" onclick="removesrc_en()" data-type="app_image">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img id="previewimage_en" class="cover-photo" onclick="$('#uploadFile_en').click();" src="{{ asset('assets/images/upload.png') }}" />
                                </div>
                                <input type="file" id="uploadFile_en" name="type_image_en" style="visibility: hidden;" accept="image/*" value="" />
                                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                            </div>
                            <span class="error"></span>
                            <div class="catimg">
                                <p class="small">Max file size: 512KB</p><br>
                                <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p><br>
                                <p class="small" style="margin-top:-30px">File dimension: 1200 x 360 pixels</p>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-job-type" id="save_data">
                        ADD
                    </button>
                    <a href="{{ route('job-type') }}" class="btn btn-default reset_style">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}

@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
</style>
@endpush
@push('scripts')
<script>
    $('#add_new_job_type').on('click', function() {
        $('#name_change').html('Add Job Type');
        $('#save_data').text('Add').button("refresh");
        $("#user_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

    $('.save-job-type').on('click', function(e) {


        $("#user_form").validate({
            rules: {
                job_type_name: {
                    required: true,
                },      
            },
            messages: {
                job_type_name: {
                    required: "Job type name required",
                },
            },
            submitHandler: function(form) {
                user_unique = $("#user_unique").val();
                $('button:submit').attr('disabled', true);
                if (user_unique) {
                    $.ajax({
                        type: 'POST',
                        url: "{{route('job-type.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        url: "{{route('job-type.store')}}",
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                }
            }
        })
    });
    function removesrc_en(){
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
    };

    $('.set_featured').on('click', function() {
    console.log($(this).data('status'));
    status = $(this).data('status');
        var cust_id = $(this).data("id");
        if (status == 'yes') {
            content = "Are you sure to remove this featured ?"
            title = "Remove featured Job type"
        } else {
            title = "Make featured Job type"
            content = "Are you sure to make this featured ?"
        }
       
        $.confirm({
            title:  title,
            content: content,
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('job-type.featured.update')}}",
                        data: {
                            id: cust_id,
                            status: status
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("job-type")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

     $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        $('#name_change').html('Edit Job Type');
        $('#save_data').text('Save').button("refresh");
        var url = "job-type/edit/";
        $.get(url + page, function(data) {
            $('#job_type_name').val(data.category.lang[0].name),
            $('#description').val(data.category.lang[0].description),
            $('#job_type_ID').val(data.category.job_type_unique_id),
            $('#job_type').val(data.category.parent_id),
            $('#user_unique').val(data.category.id)
            $('#previewimage_en').val(data.category.lang[0].image_path)
            var uploadsUrl = "<?php echo asset('/uploads/job_type') ?>";
            var imgurl = uploadsUrl + '/' + data.category.lang[0].image_path;
            if (data.category.lang[0].image_path) {
                $('#previewimage_en').attr("src", imgurl);
                $('#hidden_image_en').val(data.category.lang[0].image_path);
            }
            // $('#previewimage_ar').val(data.category.lang[1].image_path)
            // var uploadsUrls = "<?php echo asset('/uploads/category') ?>";
            // var imgurls = uploadsUrls + '/' + data.category.lang[1].image_path;
            // if (data.category.lang[1].image_path) {
            //     $('#previewimage_ar').attr("src", imgurls);
            //     $('#hidden_image_ar').val(data.category.lang[1].image_path);
            // }
            $('#user_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function() {
        var cust_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Job type',
            content: 'Are you sure to ' + act_value + ' the Job type?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('job-type.status.update')}}",
                        data: {
                            id: cust_id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("job-type")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

    
    function deleteCategory(id) {
        
        $.confirm({
            title: false,
            content: 'Are you sure to delete this job type? <br><br>You wont be able to revert this',
            
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('job-type.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("job-type")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }

</script>
@endpush