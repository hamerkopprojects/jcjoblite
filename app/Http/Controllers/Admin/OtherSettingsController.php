<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Http\Controllers\Controller;

class OtherSettingsController extends Controller
{
    public function get()
    {
        $other = OtherSettings::first();

        return view('admin.settings.other.index', compact('other'));
    }

    public function store(Request $request)
    {
        $other = OtherSettings::first();
        $data = [
            'currency' => $request->currency,
            'cancellation_time_jobber' => $request->cancellation_time_jobber,
            'cancellation_time_jobberator' => $request->cancellation_time_jobberator,
            'adverts_status' => $request->adverts_status,
            'googleads_status' => $request->googleads_status,
            'paymentgateway_status' => $request->paymentgateway_status,
        ];
        if (!empty($other)) {
            $setting_save = OtherSettings::where('id', $other->id)->update(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Common Settings Updated Successfully');
        } else {
            $setting_save = OtherSettings::Create(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Common Settings Added Successfully');
        }
    }
}
