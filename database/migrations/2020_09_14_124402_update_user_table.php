<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('verify_token')->nullable();
            $table->string('phone', 15)->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->string('user_id', 20)->nullable();
            $table->string('national_id', 20)->nullable();
            $table->enum('status', ['active', 'deactive'])->default('deactive');

            $table->foreign('role_id')
                ->references('id')
                ->on('user_role')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verify_token');
            $table->dropColumn('phone');
            $table->dropColumn('role_id');
            $table->dropColumn('user_id');
            $table->dropColumn('national_id');
            $table->dropColumn('status');
        });
    }
}
