<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_settings', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->string('currency', 20)->nullable();
            $table->string('cancellation_time_jobber', 20)->nullable();
            $table->string('cancellation_time_jobberator', 20)->nullable();
            $table->enum('adverts_status', ['active', 'deactive'])->nullable();
            $table->enum('googleads_status', ['active', 'deactive'])->nullable();
            $table->enum('paymentgateway_status', ['active', 'deactive'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_settings');
    }
}
