<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JobCategoryDetails;
use Illuminate\Support\Facades\Validator;

class JobCategoryController extends Controller
{
    public function index(Request $req)
    {
        $category = JobCategory::with('details')->get();
        $main_cat = JobCategory::with('details')
            ->where('parent_id', NULL)
            ->get();
        $search = $req->search ?? '';
        $search_catid = $req->search_catid ?? '';
        $cat_id = $req->select_maincategory ?? '';

        $query = JobCategory::query();
        if (!empty($search)) {
            $query->whereHas('details', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%")
                    ->orWhere('job_category_unique_id', 'like', "%" . $search . "%");
            });
        }
        if (!empty($cat_id)) {
            $cats = array();
            $cats = JobCategory::select('id')->with('details')
                ->where('parent_id', $cat_id)
                ->get();
            $new_array = [];
            foreach ($cats as $catss) {
                $new_array[] = $catss['id'];
            }
            $query->whereIn('parent_id', $new_array)->orWhere('parent_id', $cat_id);
        }
        $query->with('details');

        if (!empty($search_catid)) {
            $query->where('job_category_unique_id', $search_catid);
        }
        $category = $query->paginate(20);
        return view('admin.settings.job_category.index', compact('category', 'main_cat', 'search_catid', 'cat_id', 'search'));
    }
    public function store(Request $req)
    {

        $rules = [
            'job_category_name' => 'required',
            'category_image' => 'image|mimes:png,jpg,jpeg|max:2048',
            // 'description' => 'required',

        ];
        $messages = [
            'job_category_name.required' => 'Job category is required.',
            'category_image.max' => 'The image must be less than 2mb in size',
            'category_image.mimes' => "The image must be of the format jpeg or png",
            // 'type_image_en.dimensions' => "The image must be in  1200 x 360 pixels",


        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if (!empty($req->file('category_image'))) {
                $file = $req->file('category_image');
                $name_en = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/job_category/', $name_en);
            } else {
                $name_en =  NULL;
            }
            $userData = JobCategory::create([
                'job_category_unique_id' => $this->generateCategoryId(),
                'status' => "deactive",
                // 'is_featured' => "no",
                'parent_id' => $req->job_category,
            ]);

            $userData->details()->createMany([
                [
                    'name' => $req->job_category_name,
                    'description' => $req->description,
                    'image_path' => $name_en,
                    // 'language' => 'en',
                ],
            ]);

            $msg = "Job category added successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function generateCategoryId()
    {
        $lastcategory = JobCategory::select('job_category_unique_id')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastcategory) {
            $lastId = (int) substr($lastcategory->job_category_unique_id, 4);
        }

        $lastId++;

        return 'CATE' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }
    public function edit($id)
    {
        $category = JobCategory::with('details')->with('parent')->where('id', $id)->first();

        return [
            'category' => $category
        ];
    }
    public function update(Request $req)
    {

        $rules = [
            'job_category_name' => 'required',
            // 'type_image_en' => 'image|mimes:png,jpg,jpeg|max:512|dimensions:width=1200,height=360'
        ];
        $messages = [
            'job_category_name.required' => 'Job category is required.',
            // 'type_image_en.max' => 'The image must be less than 512Kb in size',
            // 'type_image_en.mimes' => "The image must be of the format jpeg or png",
            // 'type_image_en.dimensions' => "The image must be in  1200 x 360 pixels!",


        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($req->file('category_image'))) {
                $file = $req->file('category_image');
                $name_en = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/job_category/', $name_en);
            } else {
                $name_en =  $req->hidden_image_en;
            }
            $userData = JobCategory::where('id', $req->user_unique)
                ->update([
                    // 'is_featured' => "no",
                    'parent_id' => $req->job_category
                ]);

            $userData = JobCategoryDetails::where('job_cate_id', $req->user_unique)
                ->update([
                    'name' => $req->job_category_name,
                    'description' =>  $req->description,
                    'image_path' => $name_en,
                ]);

            $msg = "Job category updated successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function statusUpdate(Request $req)
    {
        $att = JobCategory::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                JobCategory::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                JobCategory::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function destroy(Request $req)
    {

        $category = JobCategory::find($req->id);
        if (!empty($category)) {

            if (!empty($category->details[0]->image_path)) {
                $path = $category->details[0]->image_path;
                $file = public_path('/uploads/job_category/' . $path);
                $img = unlink($file);
            }
            $category->details()->delete();
            $category->delete();
            $userData = JobCategory::where('parent_id', $req->id)
                ->update([
                    'parent_id' => NULL
                ]);
            return response()->json(['status' => 1, 'message' => 'Job category deleted successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }
}
