<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Sidebar extends Component
{

    public $menuArray;
    public $adminLogoArray;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->adminLogoArray = array(
            'uri' => '#',
            'img' => 'assets/images/sidelogo.png'
        );

        //details for menu
        $this->menuArray = [
            'dashboard' => [
                'position' => 1,
                'level' => 1,
                'name' => 'Dashboard',
                'icon' => 'ti-dashboard',
                'uri' => route('dashboard.get'),
            ],

            'cms' => [
                'position' => 2,
                'level' => 2,
                'name' => 'CMS',
                'icon' => 'ti-book',
                'submenus' => [
                    [
                        'name' => 'FAQ',
                        'icon' => '',
                        'uri' => route('faq.get'),
                    ],
                    [
                        'name' => 'Pages',
                        'icon' => '',
                        'uri' => route('pages.get'),
                    ],
                    // [
                    //     'name' => 'How to use the app',
                    //     'icon' => '',
                    //     // 'uri' => route('use.get'),
                    //     'uri' => '',
                    // ],

                ]
            ], 
            'users' => [
                    'position' => 6,
                    'level' => 1,
                    'name' => 'Users',
                    'icon' => 'ti-user',
                    'uri' => route('user-list.get'),
                ],
            //            'autosuggest' => [
            //                'position' => 3,
            //                'level' => 2,
            //                'name' => 'Auto Suggest',
            //                'icon' => 'ti-anchor',
            //                'submenus' => [
            //                    [
            //                        'name' => 'Task List',
            //                        'icon' => '',
            //                        'uri' => route('autosuggest', ['title' => 'task_list_item']),
            //                    ],
            //                    [
            //                        'name' => 'Job Title',
            //                        'icon' => '',
            //                        'uri' => route('autosuggest', ['title' => 'job_title']),
            //                    ],
            //                    [
            //                        'name' => 'Document',
            //                        'icon' => '',
            //                        'uri' => route('autosuggest', ['title' => 'document_to_be_submit']),
            //                    ],
            //                    [
            //                        'name' => 'Education Qualification',
            //                        'icon' => '',
            //                        'uri' => route('autosuggest', ['title' => 'education_qualification']),
            //                    ],
            //
            //                ],
            //            ],
            //            'category' => [
            //                'position' => 4,
            //                'level' => 1,
            //                'name' => 'Category',
            //                'icon' => 'ti-dashboard',
            //                'uri' => route('category'),
            //            ],

            'setting_jobs' => [
                'position' => 10,
                'level' => 2,
                'name' => 'Job Settings',
                'icon' => 'ti-anchor',
                'submenus' => [
                    [
                        'name' => 'Category',
                        'icon' => '',
                        'uri' => route('job-category'),
                    ],
                    [
                        'name' => 'Types',
                        'icon' => '',
                        //                        'uri' => route('job-type'),
                        'uri' => route('autosuggest', ['title' => 'job_type']),
                    ],
                    [
                        'name' => 'Interests',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'job_lite_interest']),
                    ],
                    [
                        'name' => 'Skill Sets',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'skill_set']),
                    ]
                ],
            ],
            'setting_general' => [
                'position' => 11,
                'level' => 2,
                'name' => 'General Settings',
                'icon' => 'ti-anchor',
                'submenus' => [
                    [
                        'name' => 'Gender',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'gender']),
                    ],                    [
                        'name' => 'Race',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'race_list']),
                    ],
                    [
                        'name' => 'Language',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'language_list']),
                    ],
                    [
                        'name' => 'Proficiency',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'proficiency']),
                    ]
                ],
            ],
            'setting_skills' => [
                'position' => 12,
                'level' => 2,
                'name' => 'Skill Settings',
                'icon' => 'ti-anchor',
                'submenus' => [
                    [
                        'name' => 'Skills',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'skills']),
                    ],
                    [
                        'name' => 'Group',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'skill_group']),
                    ],
                    [
                        'name' => 'Types',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'skill_types']),
                    ],
                    [
                        'name' => 'Level',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'skill_level']),
                    ]

                ],
            ],
            'setting_education' => [
                'position' => 13,
                'level' => 2,
                'name' => 'Education Settings',
                'icon' => 'ti-anchor',
                'submenus' => [
                    [
                        'name' => 'Highest Level',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'higer_level_education']),
                    ],
                    [
                        'name' => 'Qualification',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'education_qualification']),
                    ],
                ],
            ],
            'setting_other' => [
                'position' => 13,
                'level' => 2,
                'name' => 'Other Settings',
                'icon' => 'ti-anchor',
                'submenus' => [
                    [
                        'name' => 'Rating Segments',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'rating_segments']),
                    ],
                    [
                        'name' => 'Cancellation Reasons',
                        'icon' => '',
                        'uri' => route('autosuggest', ['title' => 'cancellation_reasons']),
                    ],
                    [
                        'name' => 'Common',
                        'icon' => '',
                        'uri' => route('other.get'),
                    ],
                ],
            ],
        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.sidebar');
    }
}
