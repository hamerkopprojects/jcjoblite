<?php

use App\Models\AppType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserAppTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        AppType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('app_type')->insert([
            [
                'name' => 'Jobber',
                'created_at' => now(),
                'updated_at' => now()

            ],
            [
                'name' => 'Jobberator',
                'created_at' => now(),
                'updated_at' => now()

            ],

        ]);
    }
}
