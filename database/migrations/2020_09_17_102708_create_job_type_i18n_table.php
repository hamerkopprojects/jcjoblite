<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobTypeI18nTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_type_i18n', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedInteger('job_type_id');
            $table->text('name')->nullable();
            $table->string('description', 255)->nullable();
            $table->text('image_path')->nullable();
            $table->enum('language', ['en']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('job_type_id')
                ->references('id')
                ->on('job_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_type_i18n');
    }
}
