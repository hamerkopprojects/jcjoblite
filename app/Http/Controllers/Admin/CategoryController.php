<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryLang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->faq_select;
        $search_field = $request->search_field ?? '';

        $title = "Category";
        $mainHeading = "CATEGORY";

        if ($search_field) {
            $data = CategoryLang::select('category_id')->where('name', 'like', "%" . $search_field . "%")
                ->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['category_id'];
            }
            $category = Category::with('lang')->whereIn('id', $da)->paginate(20);
        } else {
            $category = Category::with('lang')->paginate(20);
        }
        return view('admin.category.index', compact('category', 'search_field', 'title', 'mainHeading'));
    }
}
