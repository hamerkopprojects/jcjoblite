@extends('layouts.master')

@section('content-title')
{{ $mainHeading}}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="autosuggest_add_btn">
    <i class="ti-plus"></i> Add New {{ $title }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('category')}}" id="search-faq-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search {{ $title }}">
                            <input type="hidden" name="autosuggest_select" id="autosuggest_select">
                        </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-faq-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('category')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ $title}}</th>
                                <th>Icon</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($category) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($category as $row_data)
                            <tr>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->icon}}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit autosuggest_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white autosuggest_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $category->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="autosuggest-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"></h4>
                <input hidden id="title_hidden" value="{{ $title ?? ''}}" >
                 {{-- <input hidden id="key_value_hidden" value="{{ $keyValue ?? ''}}" > --}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-autosuggest" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                        <label>{{ $title}}</label>
                        <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter your {{ $title }}"></textarea>
                            <label class="title_en_error"></label>
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
   
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});


        $('#autosuggest_add_btn').on('click',function(e){
            $('#add-autosuggest').trigger("reset")
            heading = 'Add ' + $('#title_hidden').val();
            $('#exampleModalLabel').html(heading);
            $('#autosuggest-popup').modal({
                show:true
            })
        })

         $('.save_page').on('click',function(e){
           
            $("#add-autosuggest").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },  
                },
                messages: {               
                    title_en: {
                        required: $('#title_hidden').val() +"required",
                        
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    
                },
                 submitHandler: function(form) {
                    let edit_val=$('#id_pg').val();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('autosuggest.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            id:edit_val                  
                        },
                        success: function(result){
                            console.log(result.msg)
                            $('#autosuggest-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: $('#title_hidden').val() +'updated successfully'
                                });
                            window.location.reload();
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('autosuggest.store')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            // type : $('#key_value_hidden').val(),
                        },
    
                        success: function(result){
                            console.log(result)
                            $('#autosuggest-popup').modal('hide')
                            if(result.msg ==="success")
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: $('#title_hidden').val() +' added successfully'
                                    });
                                    window.location.reload();
                                   }else{
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }
                
                }
            });
            
           
        });

         $('.autosuggest_edit').on('click',function(e){
            e.preventDefault();
            heading = 'Edit ' + $('#title_hidden').val();
            $('#exampleModalLabel').html(heading);

            page = $(this).data('id')
            var url = "autosuggest/edit/";
        
            $.get(url  + page, function (data) {
                $('#title_en').val(data.page.lang[0].name);
                $('#id_pg').val(data.page.id)
                $('#autosuggest-popup').modal({
                    show: true

                });
            }) 
        })

          $('.change-status').on('click',function(){    
            activate = $(this).data('activate');
            $.ajax({
                        type:"POST",
                        url: "{{route('autosuggest.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });

            $('.autosuggest_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('autosuggest.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });

        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
            $.ajax( {
            url: "{{route('autosuggest.search')}}",
            method:'post',
            data: {
                search: request.term
            },
            success: function( data ) {
                response( data );
            }
            } );
        },
        minLength: 1,
        select: function( event, ui ) {
            $('#search_field').val(ui.item.label); 
            $('#autosuggest_select').val(ui.item.value); 
            return false;
        }
    });

    </script>
@endpush