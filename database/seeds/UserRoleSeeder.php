<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_role')->insert([
            [
                'name' => 'Admin ',
                'created_at' => now(),
                'updated_at' => now()

            ],
            // [
            //     'name' => 'Driver ',
            //     'created_at' => now(),
            //     'updated_at' => now()

            // ],
            // [
            //     'name' => 'Office staff ',
            //     'created_at' => now(),
            //     'updated_at' => now()

            // ],
        ]);
    }
}
